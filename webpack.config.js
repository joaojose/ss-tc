'use strict';

var path = require('path'),
    args = require('minimist')(process.argv.slice(2));
process.env.PROJECT_ENV = args.env || 'dev';
process.env.PROJECT_LANG = args.lang || 'es';

module.exports = require(path.join(__dirname, 'config/' + process.env.PROJECT_ENV));