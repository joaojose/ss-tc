// Textos em Espanhol
module.exports = {
    'title': 'Samsung - Twin Cooling Plus™',
    'description': 'Twin Cooling Plus™',
    'intro-title-slogan': 'Nuevas neveras Samsung',
    'intro-title-logo': 'Twin Cooling Plus™',
    'intro-highlight-title': 'Alimentos frescos por más tiempo',
    'intro-highlight-text': '2 evaporadores que conservan la humedad de los alimentos hasta un 70%*',
    'intro-highlight-calltoaction': 'Saiba mais',
    'videos-video-01-title': 'Assista a Campanha',
    'videos-video-01-url': 'https://www.youtube.com/watch?v=Un0ePKQfBbo',
    'videos-video-02-title': 'Veja a demonstração',
    'videos-video-02-url': 'https://www.youtube.com/watch?v=Un0ePKQfBbo',
    'twin-cooling-title-first': 'Refrigeración inteligente',
    'twin-cooling-title-second': 'Diario',
    'twin-cooling-title-third': 'Congelador + Refrigerador',
    'twin-cooling-text': 'Gran espacio para sus alimentos, bebidas y también para sus congelados. Control preciso de temperatura: la temperatura correcta todo el tiempo.'
};