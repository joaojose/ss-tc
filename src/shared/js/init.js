var MobileDetect = require('mobile-detect');
    md = new MobileDetect(window.navigator.userAgent);
function load(type, src, callback) {

    var head = document.getElementsByTagName('head')[0];
    if (type == 'script') {
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.charset = 'utf-8';
        script.async = true;

        script.src = src;
        var done = false;
        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState ||
                this.readyState === "loaded" || this.readyState === "complete") ) {
                done = true;
                if (callback) {
                    callback();
                }
                script.onload = script.onreadystatechange = null;
                if ( head && script.parentNode ) {
                    head.removeChild( script );
                }
            }
        };

        head.appendChild(script);
    } else if (type == 'style'){
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = src;
        link.media = 'all';
        head.appendChild(link);
    }
}
var $ = require('jquery'),
    imagesLoaded = require('imagesloaded'),
    magnific = require('magnific-popup');
require('magnific-popup/dist/magnific-popup.css');
imagesLoaded.makeJQueryPlugin($);
function images() {
    $('main.Containers').imagesLoaded({background: true}, function () {
        $('#loading').fadeOut();
        $('.Videos--video').magnificPopup({type:'iframe'});
    });

}
if (md.phone()) {
    load('script', 'assets/mobile.bundle.js', images);
} else {
    load('script', 'assets/desktop.bundle.js', images);
}
