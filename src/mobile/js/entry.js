require('../less/entry');

var $ = require('jquery'),
    $window = $(window);
$window.ready(function () {
    var opened = false,
        $element = $('.Containers--twin-cooling');
    function handler() {
        var scrolled = $window.scrollTop() + $window.innerHeight(),
            top = $element.position().top + ($element.height() * 0.75);
        if (opened && top > scrolled) {
            $element.removeClass('Containers--twin-cooling--aberto');
            opened = false;

        } else if (!opened && top < scrolled){
            $element.addClass('Containers--twin-cooling--aberto');
            opened = true;
        }

    }
    $(window).resize(handler).scroll(handler);
});



