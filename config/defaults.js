'use strict';
var path = require('path'),
    webpack = require('webpack'),
    CommonsChunkPlugin = require('webpack/lib/optimize/CommonsChunkPlugin'),
    ExtractTextPlugin = require('extract-text-webpack-plugin'),
    HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    srcPath: path.join(__dirname, '/../src'),
    publicPath: '/assets/',
    getDefaultPlugins: function () {
        return [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                "window.jQuery": 'jquery'
            }),
            new CommonsChunkPlugin({
                name: 'init',
                filename: 'init.bundle.js',
                chunks: ['desktop', 'mobile']
            }),
            new HtmlWebpackPlugin({
                filename:  '../index.html',
                inject: false,
                template: path.join(__dirname, '/../src/index.ejs'),
                lang: process.env.PROJECT_LANG,
                translate: require(path.join(__dirname, '/../src/shared/data/lang'))
            }),
            //new ExtractTextPlugin('[name].bundle.css')
        ];
    },
    getDefaultModules: function () {
        return {
            loaders: [
                {
                    test: /\.css$/i,
                    //loader: ExtractTextPlugin.extract('css-loader')
                    loaders: ['style-loader', 'css-loader']
                },
                {
                    test: /\.less/i,
                    //loader: ExtractTextPlugin.extract(['css-loader','less-loader'])
                    loaders: ['style-loader', 'css-loader', 'less-loader']
                },
                {
                    test: /.*\.(gif|png|jpe?g)$/i,
                    loaders: [
                        'file-loader?name=[path][name].[ext]?[hash]',
                        'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
                    ]
                },
                {
                    test: /\.(mp4|ogg|svg|woff|woff2|ttf|eot)$/i,
                    loader: 'file-loader?name=[path][name].[ext]?[hash]'
                },
                {
                    test: /\.(html)$/i,
                    loaders: [
                        'html-loader?interpolate'
                    ]
                }
            ]
        };
    }
};