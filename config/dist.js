'use strict';

var path = require('path'),
    webpack = require('webpack'),
    base = require('./base'),
    defaults = require('./defaults'),
    copyfiles = require('copyfiles');

var config = Object.assign({}, base, {
    cache: false,
    devtool: 'sourcemap',
    module: defaults.getDefaultModules(),
    plugins: defaults.getDefaultPlugins().concat([
        new webpack.optimize.DedupePlugin(),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': '"production"'
        }),
        new webpack.optimize.UglifyJsPlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.AggressiveMergingPlugin(),
        new webpack.NoErrorsPlugin()
    ])
});
config.output.path = path.join(__dirname, '/../dist/' + process.env.PROJECT_LANG + '/assets');




module.exports = config;
