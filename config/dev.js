'use strict';

var path = require('path'),
    webpack = require('webpack'),
    base = require('./base'),
    defaults = require('./defaults');


var config = Object.assign({}, base, {
    devtool: 'eval-source-map',
    module: defaults.getDefaultModules(),
    plugins: defaults.getDefaultPlugins()
});
config.output.path = path.join(__dirname, '/../src/assets/');

module.exports = config;
