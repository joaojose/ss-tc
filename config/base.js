'use strict';
var path = require('path'),
    defaults = require('./defaults');

module.exports = {
    entry: {
        'init': path.resolve(__dirname, '../src/shared/js/init.js'),
        'mobile': path.resolve(__dirname, '../src/mobile/js/entry.js'),
        'desktop': path.resolve(__dirname, '../src/desktop/js/entry.js')
    },
    debug: true,
    output: {
        filename: "[name].bundle.js",
        publicPath: '.' + defaults.publicPath
    },
    resolve: {
        alias: {
            'desktop': path.resolve(__dirname, '../src/desktop'),
            'mobile': path.resolve(__dirname, '../src/mobile'),
            'shared': path.resolve(__dirname, '../src/shared')
        }
    },
    module: {}
};
